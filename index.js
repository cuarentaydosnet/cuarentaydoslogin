const argon2 = require('argon2');
const jwt = require('jsonwebtoken');
const requestJson = require('request-json');

exports.handler = function(event,context,callback){
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    const secret = event.stageVariables.argonSecret;

  console.log("login");
  if(!event.body || !JSON.parse(event.body).password || !JSON.parse(event.body).email){
    var response = { 
          "statusCode": 403,
          "headers": {"Access-Control-Allow-Origin": "*"},
          "body": '{"mensaje" : "Login incorrecto"}',
          "isBase64Encoded": false
        };
        return callback(null,response);
  }else{
    let body = JSON.parse(event.body);
      if(body.email && body.password){
        var pass = body.password;
        var email = body.email;
      }
  }
  var query = 'q={"email" : "' + email + '"}';
  var httpClient = requestJson.createClient(db);
  httpClient.get("user?" + query + "&" + apikey,
    function(err,resMLAb,body){
      if (!err && body.length>0) {
        var hashedpass = body[0].password;
        argon2.verify(hashedpass, pass).then(match => {
          if (match) {
            console.log("COINCIDE!!!!");
            var putBody = '{"$set":{"logged":true}}';
            httpClient.put("user?" + query + "&" + apikey, JSON.parse(putBody),
              function(putErr,putMLAb,putBody){
                //nothing to do
              }
            );
             var profile = body[0];
             console.log(profile);
            var authHeader = jwt.sign(profile, secret, { expiresIn : '1h' });
            var response = { 
              "statusCode": 200,
              "headers": {
                "X-token": authHeader,
                "Access-Control-Allow-Origin": "*"
              },
              "body": '{"id" : "' + profile._id + '", "name" : "' + profile.nombre +'","token" : "' + authHeader + '", "email" : "' + profile.email + '", "tipo" : "' + profile.tipo + '" }',
              "isBase64Encoded": false
            };
            console.log(response)
            callback(null,response);
            return;
          } else {
            console.log("NO COINCIDE!!!!");
            var response = { 
              "statusCode": 403,
              "headers": {"Access-Control-Allow-Origin": "*"},
              "body": '{"mensaje" : "Login incorrecto"}',
              "isBase64Encoded": false
            };
            return callback(null,response);
          }
        }).catch(err => {
          console.log("Algo ha ido mal :( " +  err);
        });
      } else{
        var response = { 
          "statusCode": 403,
          "headers": {"Access-Control-Allow-Origin": "*"},
          "body": '{"mensaje" : "Login incorrecto"}',
          "isBase64Encoded": false
        };
        return callback(null,response);
      }
    });
};
